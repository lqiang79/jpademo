package de.listrong.jpademo.repositories;

import de.listrong.jpademo.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long>{
}
