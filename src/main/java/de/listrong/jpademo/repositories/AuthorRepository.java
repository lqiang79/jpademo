package de.listrong.jpademo.repositories;

import de.listrong.jpademo.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {
}
