package de.listrong.jpademo.repositories;

import de.listrong.jpademo.model.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher, Long>{
}
